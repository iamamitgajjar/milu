package com.example.admin.apicalldemo.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.admin.apicalldemo.Method.Login;
import com.example.admin.apicalldemo.R;
import com.example.admin.apicalldemo.Webservices.ApiHandler;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.iv_login)
    ImageView ivLogin;
    @BindView(R.id.evusername_login)
    EditText evusernameLogin;
    @BindView(R.id.evpassword_login)
    EditText evpasswordLogin;
    @BindView(R.id.btn_login)
    Button btnLogin;


    /*String url = "https://dli.tanzmed.co.tz/";*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );
        ButterKnife.bind( this );


    }

    private void LoginAPI() {

        final Call <Login> loginCall = ApiHandler.getApiService( ).getphoneID( userObject( ) );
        loginCall.enqueue( new Callback <Login>( ) {
            @Override
            public void onResponse(Call <Login> call, Response <Login> response) {
                if (response.isSuccessful( )) {
                    Login login = response.body( );
                    Log.e("new",new Gson().toJson( login ).toString());
                    int success = login.getSuccess( );
                    if (success == 1) {
                        Toast.makeText( getApplicationContext( ), " success", Toast.LENGTH_LONG ).show( );

                    } else if (success == 0) {
                        Toast.makeText( getApplicationContext( ), "not success", Toast.LENGTH_LONG ).show( );
                    }

                }
            }

            @Override
            public void onFailure(Call <Login> call, Throwable t) {
                Toast.makeText( getApplicationContext( ), "Failure success", Toast.LENGTH_LONG ).show( );

            }
        } );

    }

    private JsonObject userObject() {
        JsonObject gsonObject = new JsonObject( );
        try {
            JSONObject jsonObj_login = new JSONObject( );
            jsonObj_login.put( "device", "2" );
            jsonObj_login.put( "deviceToken", "token" );
            jsonObj_login.put( "phone", evusernameLogin.getText().toString());
            /*Log.e( "tag",evusernameLogin );*/
            jsonObj_login.put( "password", evpasswordLogin.getText().toString());
            jsonObj_login.put( "isSocial", "0" );


            JsonParser jsonParser = new JsonParser( );
            gsonObject = ( JsonObject ) jsonParser.parse( jsonObj_login.toString( ) );
            Log.e( "MY gson.JSON:  ", "AS PARAMETER  " + gsonObject );


        } catch (JSONException e) {
            e.printStackTrace( );
        }

        return gsonObject;
    }

    @OnClick(R.id.btn_login)
    public void onViewClicked() {

        String ev_phone=evusernameLogin.getText().toString();
        String ev_password=evpasswordLogin.getText().toString();
        LoginAPI();
    }


    /*public void getRetrofitObject() {

        Retrofit  retrofit=new Retrofit.Builder()
               .baseUrl( url )
                .addConverterFactory( GsonConverterFactory.create() )
                .build();



        RetrofitObject service=retrofit.create(RetrofitObject.class);
        Call<Result> logincall= service.getuserID();
       *//* Call<Result> logincall= service.getPhone();*//*
        logincall.enqueue( new Callback <Result>( ) {
            @Override
            public void onResponse(Call <Result> call, Response<Result> response) {

                Log.e( "tag","response"+response.toString() );
                Log.e( "tag","response"+response.body().getUserId() );

            }

            @Override
            public void onFailure(Call <Result> call, Throwable t) {
                Log.e( "error",t.toString() );

            }
        } );
*/

}
